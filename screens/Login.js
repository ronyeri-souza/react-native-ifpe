import { StyleSheet, Text, View } from "react-native";
import { Avatar, Input, Button } from "react-native-elements";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { useState } from "react";
import authenticateOnFirebase from "../FirebaseAuth";

export default function Login({ navigation }) {
  authenticateOnFirebase();

  const [getEmail, setEmail] = useState();
  const [getPassword, setPassword] = useState();

  function login() {
    const auth = getAuth();
    signInWithEmailAndPassword(auth, getEmail, getPassword)
      .then((userCredentials) => {
        const user = userCredentials.user;
        console.log(user);
        navigation.navigate("Contacts");
      })
      .catch((error) => {
        console.error(error);
      });
  }

  return (
    <View style={styles.container}>
      <Avatar
        size={100}
        rounded
        icon={{ name: "user", type: "font-awesome" }}
        containerStyle={{ backgroundColor: "#0275d8" }}
      ></Avatar>

      <View style={styles.input}>
        <Input placeholder="E-mail" onChangeText={(email) => setEmail(email)} />
        <Input
          placeholder="Senha"
          secureTextEntry={true}
          inputStyle={{ marginTop: 20 }}
          onChangeText={(password) => setPassword(password)}
        />
        <Button
          title={"Entrar"}
          buttonStyle={{ backgroundColor: "#5cb85c", height: 60, width: 380 }}
          containerStyle={{ alignItems: "center" }}
          onPress={() => login()}
        />
        <Button
          title={"Cadastro"}
          buttonStyle={{
            backgroundColor: "#0275d8",
            height: 50,
            width: 380,
            marginTop: 20,
          }}
          containerStyle={{ alignItems: "center" }}
          onPress={() => navigation.navigate("Register")}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },

  input: {
    marginTop: 40,
    width: 400,
  },
});
