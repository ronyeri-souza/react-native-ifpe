import axios from "axios";
import { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Input, Button, Header } from "react-native-elements";

export default function ContactRegister({ navigation }) {
  const [getName, setName] = useState();
  const [getPhone, setPhone] = useState();
  const [getEmail, setEmail] = useState();

  function register() {
    axios
      .post("http://professornilson.com/testeservico/clientes", {
        nome: getName,
        telefone: getPhone,
        email: getEmail,
      })
      .then(function (response) {
        console.log(response.config.data);
        navigation.navigate("Contacts");
        console.log("Contato cadastrado com sucesso");
        console.log(getEmail);
      })
      .catch(function (error) {
        alert("Erro ao cadastrar contato");
        console.error(error);
        console.error("Falha no cadastro do contato");
      });
  }

  return (
    <>
      <Header
        containerStyle={{ height: 100, backgroundColor: "#fff" }}
        leftComponent={{
          icon: "arrow-left",
          type: "font-awesome",
          color: "#0275d8",
          iconStyle: { color: "#000", fontSize: 30 },
          onPress: () => navigation.navigate("Contacts"),
        }}
      />
      <View style={styles.container}>
        <Text style={{ fontSize: 30 }}>Cadastro de contato</Text>
        <View style={styles.input}>
          <Input placeholder="Nome" onChangeText={(text) => setName(text)} />
          <Input
            placeholder="E-mail"
            inputStyle={{ marginTop: 20 }}
            onChangeText={(text) => setEmail(text)}
          />
          <Input
            placeholder="Telefone"
            inputStyle={{ marginTop: 20 }}
            onChangeText={(text) => setPhone(text)}
          />
          <Button
            onPress={() => register()}
            title={"Salvar"}
            buttonStyle={{ backgroundColor: "#5cb85c", height: 60, width: 380 }}
            containerStyle={{ alignItems: "center" }}
          />
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },

  input: {
    marginTop: 40,
    width: 400,
  },
});
