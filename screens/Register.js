import { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Input, Button, Header } from "react-native-elements";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import authenticateOnFirebase from "../FirebaseAuth";

export default function Register({ navigation }) {
  authenticateOnFirebase();

  const [getEmail, setEmail] = useState();
  const [getPassword, setPassword] = useState();

  function createUser() {
    const auth = getAuth();
    createUserWithEmailAndPassword(auth, getEmail, getPassword)
      .then((userCredentials) => {
        const user = userCredentials.user;
        console.log(user);
        navigation.navigate("Login");
      })
      .catch((error) => {
        console.error(error);
      });
  }

  return (
    <>
      <Header
        containerStyle={{ height: 100, backgroundColor: "#fff" }}
        leftComponent={{
          icon: "arrow-left",
          type: "font-awesome",
          color: "#0275d8",
          iconStyle: { color: "#000", fontSize: 30 },
          onPress: () => navigation.navigate("Login"),
        }}
      />
      <View style={styles.container}>
        <Text style={{ fontSize: 30 }}>Cadastro</Text>
        <View style={styles.input}>
          <Input
            placeholder="E-mail"
            inputStyle={{ marginTop: 20 }}
            onChangeText={(email) => setEmail(email)}
          />
          <Input
            placeholder="Senha"
            secureTextEntry={true}
            inputStyle={{ marginTop: 20 }}
            onChangeText={(password) => setPassword(password)}
          />
          <Button
            title={"Salvar"}
            onPress={() => createUser()}
            buttonStyle={{ backgroundColor: "#5cb85c", height: 60, width: 380 }}
            containerStyle={{ alignItems: "center" }}
          />
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },

  input: {
    marginTop: 40,
    width: 400,
  },
});
