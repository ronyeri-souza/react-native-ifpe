import axios from "axios";
import { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Input, Button, Header } from "react-native-elements";

export default function ContactEdit({ route, navigation }) {
  const [getName, setName] = useState();
  const [getCpf, setCpf] = useState();
  const [getPhone, setPhone] = useState();
  const [getId, setId] = useState();
  const [getEmail, setEmail] = useState();

  useEffect(() => {
    if (route.params) {
      const { name } = route.params;
      const { phone } = route.params;
      const { id } = route.params;
      const { email } = route.params;
      const { cpf } = route.params;

      setName(name);
      setPhone(phone);
      setCpf(cpf);
      setId(id);
      setEmail(email);
    }
  }, []);

  function edit() {
    axios
      .put("http://professornilson.com/testeservico/clientes/" + getId, {
        nome: getName,
        telefone: getPhone,
        email: getEmail,
        cpf: getCpf,
      })
      .then(function (response) {
        console.log(response.config.data);
        navigation.navigate("Contacts");
        console.log("Contato editado com sucesso");
      })
      .catch(function (error) {
        console.error(error);
        console.error("Falha ao editar contato");
      });
  }

  function remove() {
    axios
      .delete("http://professornilson.com/testeservico/clientes/" + getId)
      .then(function (response) {
        console.log(response);
        navigation.navigate("Contacts");
        console.log("Contato excluído com sucesso");
      })
      .catch(function (error) {
        console.error(error);
        console.error("Falha na exclusão do contato");
      });
  }

  return (
    <>
      <Header
        containerStyle={{ height: 100, backgroundColor: "#fff" }}
        leftComponent={{
          icon: "arrow-left",
          type: "font-awesome",
          color: "#0275d8",
          iconStyle: { color: "#000", fontSize: 30 },
          onPress: () => navigation.navigate("Contacts"),
        }}
      />
      <View style={styles.container}>
        <Text style={{ fontSize: 30 }}>Editar contato</Text>
        <View style={styles.input}>
          <Input
            placeholder="Nome"
            onChangeText={(text) => setName(text)}
            value={getName || ""}
          />
          <Input
            placeholder="E-mail"
            inputStyle={{ marginTop: 20 }}
            onChangeText={(text) => setEmail(text)}
            value={getEmail}
          />
          <Input
            placeholder="Telefone"
            inputStyle={{ marginTop: 20 }}
            onChangeText={(text) => setPhone(text)}
            value={getPhone || ""}
          />
          <Button
            onPress={() => edit()}
            title={"Salvar"}
            buttonStyle={{
              backgroundColor: "#5cb85c",
              height: 60,
              width: 380,
              marginBottom: 10,
            }}
            containerStyle={{ alignItems: "center" }}
          />

          <Button
            onPress={() => remove()}
            title={"Excluir"}
            buttonStyle={{ backgroundColor: "#d94141", height: 60, width: 380 }}
            containerStyle={{ alignItems: "center" }}
          />
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },

  input: {
    marginTop: 40,
    width: 400,
  },
});
