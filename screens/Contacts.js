import { getAuth, signOut } from "@firebase/auth";
import axios from "axios";
import { useEffect, useState } from "react";
import { StyleSheet, ScrollView } from "react-native";
import { Avatar, Header, ListItem } from "react-native-elements";
import authenticateOnFirebase from "../FirebaseAuth";

export default function Contacts({ navigation }) {
  authenticateOnFirebase();

  const [getContacts, setContacts] = useState([]);

  function logout() {
    const auth = getAuth();
    signOut(auth)
      .then(() => {
        console.log("Successfully on logout");
        navigation.navigate("Login");
      })
      .catch((error) => {
        console.error(error);
      });
  }

  function listContacts() {
    axios
      .get("http://professornilson.com/testeservico/clientes")
      .then(function (response) {
        setContacts(response.data);
      })
      .catch(function (error) {
        console.error(error);
      });
  }

  useEffect(() => {
    listContacts();
  }, [getContacts]);

  return (
    <>
      <Header
        containerStyle={{ height: 100, backgroundColor: "#fff" }}
        centerComponent={{
          text: "Meus contatos",
          style: { fontSize: 20 },
        }}
        leftComponent={{
          icon: "plus",
          type: "font-awesome",
          color: "#0275d8",
          iconStyle: { color: "#000", fontSize: 30 },
          onPress: () => navigation.navigate("ContactRegister"),
        }}
        rightComponent={{
          icon: "sign-out",
          type: "font-awesome",
          color: "#0275d8",
          iconStyle: { color: "#000", fontSize: 30 },
          onPress: () => logout(),
        }}
      />
      <ScrollView>
        {getContacts.map((contact) => (
          <ListItem
            key={contact.id}
            bottomDivider
            onPress={() =>
              navigation.navigate("ContactEdit", {
                name: contact.nome,
                phone: contact.telefone,
                email: contact.email,
                cpf: contact.cpf,
                id: contact.id,
              })
            }
          >
            <Avatar
              size={70}
              rounded
              icon={{ name: "user", type: "font-awesome" }}
              containerStyle={{ backgroundColor: "#0275d8" }}
            ></Avatar>
            <ListItem.Content>
              <ListItem.Title>{contact.nome}</ListItem.Title>
              <ListItem.Subtitle>{contact.telefone}</ListItem.Subtitle>
            </ListItem.Content>
          </ListItem>
        ))}
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },

  input: {
    marginTop: 40,
    width: 400,
  },
});
