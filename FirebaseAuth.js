import { initializeApp } from "@firebase/app";

export default function authenticateOnFirebase() {
    
    const firebaseConfig = {
        apiKey: "AIzaSyC1eydsLNrSLB0p3LnTSZOLPLzJRQz0ZUs",
        authDomain: "aula-app-4313b.firebaseapp.com",
        projectId: "aula-app-4313b",
        storageBucket: "aula-app-4313b.appspot.com",
        messagingSenderId: "614123031994",
        appId: "1:614123031994:web:0fbef6629e14bd48fc6183",
      };
    
      const app = initializeApp(firebaseConfig);
}