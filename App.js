import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import ContactEdit from "./screens/ContactEdit";
import ContactRegister from "./screens/ContactRegister";
import Contacts from "./screens/Contacts";
import Login from "./screens/Login";
import Register from "./screens/Register";

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{ headerShown: false }}
      >
        <Stack.Screen name="Login" component={Login}></Stack.Screen>
        <Stack.Screen name="Register" component={Register}></Stack.Screen>
        <Stack.Screen name="Contacts" component={Contacts}></Stack.Screen>
        <Stack.Screen
          name="ContactRegister"
          component={ContactRegister}
        ></Stack.Screen>
        <Stack.Screen name="ContactEdit" component={ContactEdit}></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
